<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api;

use Adduc\Stitcher\Client;

abstract class Action
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    abstract public function __invoke();
}
