<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api;

class Parameters
{
    const MODE_WEBSITE = 'website';
    const MODE_PHONE = 'phone';

    /**
     * @property float
     */
    public $version = 4.19;

    /**
     * @property int
     */
    public $uid;

    /**
     * On Android, the API level
     *
     * @property int
     */
    public $os = 25;

    /**
     * @property string
     */
    public $mode = 'android';

    /**
     * @property string
     */
    public $deviceType = 'phone';

    /**
     * @property int
     */
    public $hiRes = 1;

    /**
     * @property string
     */
    public $udid;

    /**
     * @property string
     */
    public $androidId;

    /**
     * @property int
     */
    public $timezone;

    /**
     * @property string
     */
    public $connectionType;

    /**
     * @property array
     */
    public $guzzle_options;

    /**
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function __set($key, $value)
    {
        throw new \DomainException("Parameter doesn't exist.");
    }

    /**
     * @return array
     */
    public function toArray()
    {
        // This may cause trouble if private/protected attributes are
        // ever declared.
        return get_object_vars($this);
    }
}
