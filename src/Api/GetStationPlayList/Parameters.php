<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetStationPlayList;

use Adduc\Stitcher\Api;

class Parameters extends Api\Parameters
{
    /**
     * @property int
     */
    public $bgFetch;

    /**
     * Station ID
     *
     * @property int
     */
    public $sid;

    /**
     * @property int
     */
    public $lid;

    /**
     * @property int
     */
    public $c;
}
