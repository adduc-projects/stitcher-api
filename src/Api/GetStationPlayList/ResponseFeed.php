<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetStationPlayList;

use Adduc\Stitcher\Api;

class ResponseFeed extends Api\Response
{
    /**
     * @property int
     */
    public $id;

    /**
     * @property string
     */
    public $name;

    /**
     * @property
     */
    public $feedDescription;

    /**
     * @property
     */
    public $title;

    /**
     * @property
     */
    public $episodeImage;

    /**
     * @property
     */
    public $episodeDescription;

    /**
     * @property
     */
    public $dateString;

    /**
     * Was genre.id, changed to genre_id for PHP compatibility.
     *
     * @property int
     */
    public $genre_id;

    /**
     * Was genre.name, changed to genre_name for PHP compatibility.
     *
     * @property string
     */
    public $genre_name;

    /**
     * Was genre.color, changed to genre_color for PHP compatibility.
     *
     * @property string
     */
    public $genre_color;

    /**
     * @property string
     */
    public $thumbnailURL;

    /**
     * @property string
     */
    public $smallThumbnailURL;

    /**
     * @property string
     */
    public $largeThumbnailURL;

    /**
     * @property string
     */
    public $seokey;

    /**
     * @property int
     */
    public $id_Episode;

    /**
     * @property string
     */
    public $description;

    /**
     * @property int
     */
    public $cached;

    /**
     * @property string
     */
    public $episodeURL;

    /**
     * @property string
     */
    public $episodeURL_original;

    /**
     * @property int
     */
    public $duration;

    /**
     * @property int
     */
    public $commentCount;

    /**
     * @property ?
     */
    public $noReport;

    /**
     * @property ?
     */
    public $authRequired;

    /**
     * @property ?
     */
    public $authMethod;

    /**
     * @property int
     */
    public $skippable;

    /**
     * @property string
     */
    public $published;

    /**
     * @property int
     */
    public $upRating;

    /**
     * @property int
     */
    public $displayAds;

    /**
     * @property int
     */
    public $adsAllowed;

    /**
     * @property int
     */
    public $id_RSSProvider;

    /**
     * @property ?
     */
    public $color;

    /**
     * @property int
     */
    public $explicit;

    /**
     * @property int
     */
    public $bitrate;

    /**
     * @property ?
     */
    public $headerSizeInBytes;

    public $banner;

    public $id_RSSFeed_premium;

    public $episode;

    public $isFavorite;

    public $fave_lid;

    public $freemium;

    /**
     * @property ResponseFeedSeason[]
     */
    public $seasons = [];

    /**
     * @property ResponseFeedMarker[]
     */
    public $markers = [];
}
