<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetStationPlayList;

use Adduc\Stitcher\Api;

class Action extends Api\Action
{
    /**
     * @param Parameters $parameters
     * @return Response
     */
    public function __invoke(Parameters $parameters = null)
    {
        if (is_null($parameters)) {
            $parameters = new Parameters();
        }

        // must be GET, cannot be POST :/
        $method = "GET";
        $uri = "/Service/GetStationPlayList.php";
        $options = ['query' => $parameters->toArray()];

        if (!empty($parameters->guzzle_options)) {
            $options += $parameters->guzzle_options;
            unset($options['guzzle_options']);
        }

        $response_raw = $this->client->request($method, $uri, $options);
        $body = (string)$response_raw->getBody();
        $error_pos = strpos($body, "<error code=-1 ");

        // On error, Stitcher may return incorrect XML. Correct it.
        if ($error_pos !== false) {
            $body = substr($body, 0, $error_pos)
                . '<error code="-1" '
                . substr($body, $error_pos + 15);
        }

        $response_xml = simplexml_load_string($body);
        $response_user = new Response($response_xml);

        return $response_user;
    }
}
