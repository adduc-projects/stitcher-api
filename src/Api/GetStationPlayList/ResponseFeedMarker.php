<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetStationPlayList;

use Adduc\Stitcher\Api;

class ResponseFeedMarker extends Api\Response
{
    public $id;
    public $heard;
    public $autoGenOffset;
    public $offset;
}
