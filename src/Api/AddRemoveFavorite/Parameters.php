<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\AddRemoveFavorite;

use Adduc\Stitcher\Api;

class Parameters extends Api\Parameters
{
    /**
     * @property ?
     */
    public $fbid;

    /**
     * @example 'not_linked'
     * @property string
     */
    public $access_token;

    /**
     * @property int
     */
    public $fid;

    /**
     * Underscore delimited list of list IDs
     * If empty, deletes feed from all lists
     * @property string
     */
    public $lid;

    /**
     * @property int
     */
    public $postAttempt;
}
