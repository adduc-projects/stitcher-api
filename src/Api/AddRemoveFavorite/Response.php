<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\AddRemoveFavorite;

use Adduc\Stitcher\Api;

class Response extends Api\Response
{
    public $error;
}
