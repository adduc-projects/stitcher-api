<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\AddRemoveFavorite;

use Adduc\Stitcher\Api;

class Action extends Api\Action
{
    /**
     * @param Parameters $parameters
     * @return Response
     */
    public function __invoke(Parameters $parameters = null)
    {
        if (is_null($parameters)) {
            $parameters = new Parameters();
        }

        $method = "POST";
        $uri = "/Service/AddRemoveFavorite.php";
        $options = ['form_params' => $parameters->toArray()];

        if (!empty($parameters->guzzle_options)) {
            $options += $parameters->guzzle_options;
            unset($options['guzzle_options']);
        }

        $response_raw = $this->client->request($method, $uri, $options);
        $response_xml = simplexml_load_string((string)$response_raw->getBody());
        $response_user = new Response($response_xml);

        return $response_user;
    }
}
