<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetPage;

use Adduc\Stitcher\Api;

class ResponseCarousel extends Api\Response
{
    public $id;
    public $name;
    public $height;
    public $width;
    public $cards = [];
}
