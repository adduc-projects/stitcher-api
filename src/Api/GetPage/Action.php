<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetPage;

use Adduc\Stitcher\Api;
use GuzzleHttp\Exception\ServerException;

class Action extends Api\Action
{
    /**
     * @param Parameters $parameters
     * @return Response
     */
    public function __invoke(Parameters $parameters = null)
    {
        if (is_null($parameters)) {
            $parameters = new Parameters();
        }

        // must be GET, cannot be POST :/
        $method = "GET";
        $uri = "/Service/GetPage.php";
        $options = ['query' => $parameters->toArray()];

        if (!empty($parameters->guzzle_options)) {
            $options += $parameters->guzzle_options;
            unset($options['guzzle_options']);
        }

        try {
            $response_raw = $this->client->request($method, $uri, $options);

            $callback = function ($matches) {
                return $matches[1] . htmlspecialchars($matches[2]) . $matches[3];
            };

            // Stitcher does not escape the page description, and can
            // cause invalid XML if the description contains an
            // ampersand (&).
            $response_raw = preg_replace_callback(
                '/(<page[^>]*description=")(.*)("[^>]*>)/m',
                $callback,
                $response_raw->getBody()
            );

            $response_xml = simplexml_load_string($response_raw);
        } catch (ServerException $e) {
            // Stitcher returns a 500 error when requesting a
            // page without passing in a valid user ID.
            $xml = '<result error="userNotFound"/>';
            $response_xml = simplexml_load_string($xml);
        }

        $response_user = new Response($response_xml);

        return $response_user;
    }
}
