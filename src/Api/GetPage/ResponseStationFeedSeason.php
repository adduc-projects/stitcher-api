<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetPage;

use Adduc\Stitcher\Api;

class ResponseStationFeedSeason extends Api\Response
{
    public $id;
    public $seasonNumber;
    public $title;
    public $description;
}
