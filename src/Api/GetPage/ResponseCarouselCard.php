<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetPage;

use Adduc\Stitcher\Api;

class ResponseCarouselCard extends Api\Response
{
    public $id;
    public $title;
    public $banner;
    public $imageURL;
    public $link;
    public $feed;
}
