<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetPage;

use Adduc\Stitcher\Api;

class Response extends Api\Response
{
    public $error;
    public $id;
    public $description;
    public $timestamp;
    public $carousels = [];
    public $stations = [];
}
