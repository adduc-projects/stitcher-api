<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetPage;

use Adduc\Stitcher\Api;

class ResponseStation extends Api\Response
{
    public $id;
    public $name;
    public $subtitle;
    public $keywords;
    public $explicit;
    public $smallThumbnailURL;
    public $sponsored;
    public $feeds = [];
    public $mastheadImageURL;
    public $synonyms;
}
