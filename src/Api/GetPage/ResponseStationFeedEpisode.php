<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetPage;

use Adduc\Stitcher\Api;

class ResponseStationFeedEpisode extends Api\Response
{
    public $id;
    public $duration;
    public $episodeImage;
    public $published;
    public $dateString;
    public $headerSizeInBytes;
    public $bitrate;
    public $url;
    public $episodeURL_original;
    public $banner;
    public $title;
    public $description;
    public $unlisted;
    public $id_Season;
    public $episodeNumber;
    public $commentCount;
    public $id_RSSEpisode_parent;
    public $noCache;
    public $originalDescription;
}
