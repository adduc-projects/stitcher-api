<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\Search;

use Adduc\Stitcher\Api;

class Response extends Api\Response
{
    public $term;
    public $searchType;
    public $method;
    public $user;
    public $results;
    public $feeds = [];
}
