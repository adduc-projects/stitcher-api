<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\Search;

use Adduc\Stitcher\Api;

class ResponseResults extends Api\Response
{
    public $total;
}
