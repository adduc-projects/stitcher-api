<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\Search;

use Adduc\Stitcher\Api;

class ResponseFeed extends Api\Response
{
    public $id;
    public $dateString;
    public $thumbnailURL;
    public $smallThumbnailURL;
    public $largeThumbnailURL;
    public $seokey;
    public $explicit;
    public $skippable;
    public $published;
    public $relevanceScore;
    public $upRating;
    public $luceneReferences;
    public $displayAds;
    public $adsAllowed;
    public $id_RSSProvider;
    public $freemium;
    public $color;
    public $name;
    public $description;
    public $banner;
    public $authMethod;
    public $id_RSSFeed_premium;
    public $authRequired;
    public $marker;
    public $isFavorite;
    public $fave_lid;
    public $donationURL;
    public $episodeCount;
    public $imageURL;
    public $premium;
    public $bitrate;
    public $streamURL;
    public $playerImageURL;
    public $totalDuration;
    public $autoSeason;
    public $sortDirection;

    /** @var ResponseFeedSeason[] */
    public $seasons = [];

    /** @var ResponseFeedEpisode[] */
    public $episodes = [];
}
