<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\Search;

use Adduc\Stitcher\Api;

class ResponseUser extends Api\Response
{
    public $id;
}
