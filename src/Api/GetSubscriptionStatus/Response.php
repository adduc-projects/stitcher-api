<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetSubscriptionStatus;

use Adduc\Stitcher\Api;

class Response extends Api\Response
{
    /**
     * Stitcher User ID
     *
     * @property int
     */
    public $uid;

    /**
     * Indicates status of subscription (whether user can access premium
     * content)
     *
     * 0: Not Subscribed
     *
     * @property int
     */
    public $subscriptionState;

    /**
     * Date/Time subscription will expire
     *
     * @todo determine which timezone (appears to be PST)
     *
     * @property string
     */
    public $subscriptionExpiration;

    /**
     * @example web
     * @property string
     */
    public $subscriptionPlatform;
}
