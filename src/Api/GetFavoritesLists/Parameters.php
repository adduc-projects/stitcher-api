<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetFavoritesLists;

use Adduc\Stitcher\Api;

class Parameters extends Api\Parameters
{
    /**
     * @property int
     */
    public $please;

    /**
     * @property int
     */
    public $liveList;
}
