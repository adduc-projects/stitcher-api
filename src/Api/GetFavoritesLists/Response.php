<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetFavoritesLists;

use Adduc\Stitcher\Api;

class Response extends Api\Response
{
    /**
     * @property ResponseList
     */
    public $lists = [];
}
