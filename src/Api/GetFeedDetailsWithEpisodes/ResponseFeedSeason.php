<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetFeedDetailsWithEpisodes;

use Adduc\Stitcher\Api;

class ResponseFeedSeason extends Api\Response
{
    public $id;
    public $seasonNumber;
    public $title;
    public $description;
}
