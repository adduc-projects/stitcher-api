<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetFeedDetailsWithEpisodes;

use Adduc\Stitcher\Api;

class ResponseEpisodesMarker extends Api\Response
{
    public $id;
    public $offset;
    public $autoGenOffset;
    public $heard;
}
