<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\LoadUserFromID;

use Adduc\Stitcher\Api;

class Parameters extends Api\Parameters
{
    /**
     * @property int
     */
    public $markStartup;
}
