<?php declare(strict_types=1);

namespace Adduc\Stitcher;

use GuzzleHttp;

class Client extends GuzzleHttp\Client
{
    /**
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        if (!isset($config['base_uri'])) {
            $config['base_uri'] = 'https://www.stitcher.com';
        }

        if (!isset($config['handler'])) {
            $config['handler'] = GuzzleHttp\HandlerStack::create();
        } elseif (!is_callable($config['handler'])) {
            throw new \InvalidArgumentException('handler must be a callable');
        }

        $middlewares = [
            new Middleware\Encrypt($config),
        ];

        foreach ($middlewares as $middleware) {
            $config['handler']->unshift($middleware);
        }

        parent::__construct($config);
    }

    /**
     * Invokes an Action, if it exists.
     *
     * @param string $name
     * @param array $arguments
     */
    public function __call($name, $arguments)
    {
        $namespace = __NAMESPACE__ . "\\Api\\{$name}";
        $class = $namespace . "\\Action";

        if (!class_exists($class)) {
            return parent::__call($name, $arguments);
        }

        $action = new $class($this);
        $class = $namespace . "\\Parameters";

        $parameters = null;

        if (isset($arguments[0])) {
            if ($arguments[0] instanceof $class) {
                $parameters = $arguments[0];
            } else {
                $parameters = new $class($arguments[0]);
            }
        }

        return $action($parameters);
    }
}
