<?php declare(strict_types=1);

use Adduc\Stitcher;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/config.php';

$client = new Stitcher\Client([
    'stitcher' => [
        'key' => $config['encrypt-key']
    ]
]);

$result = $client->GetStationPlayList([
    'uid' => $config['user-id'],
    'sid' => 1,
    'lid' => $config['station-id'],
]);

echo "\n\n";

if (PHP_SAPI != 'cli') {
    echo "<br><pre>";
}

print_r($result);
