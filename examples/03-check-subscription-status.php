<?php declare(strict_types=1);

use Adduc\Stitcher;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/config.php';

$client = new Stitcher\Client([
    'stitcher' => [
        'key' => $config['encrypt-key']
    ]
]);

$result = $client->GetSubscriptionStatus([
    'uid' => $config['user-id']
]);

if ($result->subscriptionState) {
    echo "This is a subscribing user.";
} else {
    echo "This is not a subscribing user.";
}

echo "\n\n";

if (PHP_SAPI != 'cli') {
    echo "<br><pre>";
}

print_r($result);
