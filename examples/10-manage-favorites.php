<?php declare(strict_types=1);

use Adduc\Stitcher;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/config.php';

echo PHP_SAPI == 'cli' ? "\n\n" : '<br><pre>';

$client = new Stitcher\Client([
    'stitcher' => [
        'key' => $config['encrypt-key']
    ]
]);

// Add feed to list
$result = $client->AddRemoveFavorite([
    'uid' => $config['user-id'],
    'fid' => $config['feed-id'],
    'lid' => $config['station-id'],
]);

print_r($result);

// Verify it's added
$result = $client->GetStationPlayList([
    'uid' => $config['user-id'],
    'sid' => 1,
    'lid' => $config['station-id'],
]);

print_r($result);

// Remove feed from list
$result = $client->AddRemoveFavorite([
    'uid' => $config['user-id'],
    'fid' => $config['feed-id'],
    'lid' => '',
]);

print_r($result);

// Verify it's removed
$result = $client->GetStationPlayList([
    'uid' => $config['user-id'],
    'sid' => 1,
    'lid' => $config['station-id'],
]);

print_r($result);
