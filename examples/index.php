<?php declare(strict_types=1);

$files = glob('*-*.php');

foreach ($files as $file) {
    echo "<a href='{$file}'>{$file}</a><br>";
}
